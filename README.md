# CS373: Software Engineering Collatz Repo

* Name: Toby Liu

* EID: TL24874

* GitLab ID: tobytliu

* HackerRank ID: tobytliu

* Git SHA: 9dee2ce6daa1e9a2f564082141ecff528a30f7dc

* GitLab Pipelines: https://gitlab.com/tobytliu/cs373-collatz/pipelines

* Estimated completion time: 8 hours

* Actual completion time: 11

* Comments: figuring out everything besides the literal code took much longer than I thought it would
